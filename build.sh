#!/bin/bash 
set -e
VERSION=v2.0.4
IMAGE=test/static-apps
# redmine pwd
docker login hub.eole.education
docker build -t hub.eole.education/$IMAGE:$VERSION .
docker push hub.eole.education/$IMAGE:$VERSION
docker tag hub.eole.education/$IMAGE:$VERSION hub.eole.education/$IMAGE:latest
echo "docker run -p 8080:80 hub.eole.education/$IMAGE:$VERSION hub.eole.education/$IMAGE:latest"

